bin/pscopy: src/pscopy.c
	gcc -Wall $< -o $@
.PHONY: clean
clean:
	rm bin/pscopy
