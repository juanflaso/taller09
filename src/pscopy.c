#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main (int argc, char **argv) {
    umask(0);
    
    char *origin = argv[1];
    char *destiny = argv[2];
    int bytesOrigin;
    char buffer[1000];

    int fileOrigin = open(origin, O_RDONLY,0);
    int fileDestiny = open(destiny, O_WRONLY|O_TRUNC, 0);
    
    if( fileOrigin < 0 || fileDestiny < 0 ){
            perror("Error: ");
            return -1;
    }
    bytesOrigin = read(fileOrigin, buffer,1000);
    while (bytesOrigin != 0){
        write(fileDestiny,buffer,bytesOrigin);
        bytesOrigin = read(fileOrigin, buffer,1000);
    }

    close(fileDestiny);
    close(fileOrigin);
    return 0;
}
